# Semestrální práce PLI - Jan Procházka

Program detekuje jazyk zkoumaného textu. Konkrétně CZ/SK

## Funkčnost

Program v CZ a SK korpus najde všechny použité znaky a sestaví z nich model. Následně se model těmito korpusy natrénuje.

Pokud ve zkoumaném textu najde znak, který nemá v modelu, přidá tento znak do abecedy, vytvoří nový model a natrénuje ho na korpusu.
To je ovšem časově velmi náročné. Pokud by byla potřeba tuto možnost vypnout, stačí zakomentovat řádek 133. Program pak neznámé triplety ignoruje.

## Testovací data
Program nejříve určí jazyk pro vozrové texty (idnes, zoznam). Následně umožňuje v cyklu zadávat do příkazové řádky vlastní text. Ukončení programu bude vykonáno při zadání prázdného textu.

Při odkomentování řádků 228 - 260 dojde po ukončení ručního zadávání ke zpracování korpusů a textů bez diakritiky.
