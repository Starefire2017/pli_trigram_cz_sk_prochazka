import math
import json as json
from collections import Counter, defaultdict
import re
from pip._vendor.msgpack.fallback import xrange

alphabet = []
CZ_trigram = {}
SK_trigram = {}
CZ_corpus = 'CZ.txt'
SK_corpus = 'SK.txt'


def make_alphabet(CZ_file, SK_file):
    l_alphabet = []
    txt = '#'
    with open(CZ_file, 'r', encoding='windows-1250', errors='ignore') as f:
        txt += f.read().lower()
        # chars = list(txt)
        # l_alphabet.append(chars)
    with open(SK_file, 'r', encoding='windows-1250', errors='ignore') as f:
        txt += f.read().lower()
        # chars = list(txt)
        # l_alphabet.append(chars)
    l_alphabet = list(dict.fromkeys(txt))
    global alphabet
    alphabet = l_alphabet


def make_trigram():
    global alphabet

    #print('abeceda obsahuje ' + str(len(alphabet)) + ' znaku')
    trigram = {}
    l3_trigram = {}
    for a in alphabet:
        for b in alphabet:
            for c in alphabet:
                if c != '#':
                    l3_trigram[c] = 0
            trigram[a+b] = l3_trigram
            l3_trigram = {}
    return trigram


def learn_trigram(CZ_filename, SK_filename):
    global CZ_trigram
    global SK_trigram
    CZ_trigram = make_trigram()
    # print('Vytvoren CZ model')
    SK_trigram = make_trigram()
    # print('Vytvoren SK model')

    with open(CZ_filename, 'r', encoding='windows-1250', errors='ignore') as f:
        # print("ucim CZ model")
        txt = f.read().lower()
        txt = escape_read(txt)
        chars = list(txt)
        corpus = ['#', '#'] + chars
        for i in xrange(2, len(corpus)):
            key = corpus[i - 2] + corpus[i - 1]
            CZ_trigram[key][corpus[i]]+=1

    with open(SK_filename, 'r', encoding='windows-1250', errors='ignore') as f:
        # print("ucim SK model")
        txt = f.read().lower()
        txt = escape_read(txt)
        chars = list(txt)
        corpus = ['#', '#'] + chars
        for i in xrange(2, len(corpus)):
            key = corpus[i - 2] + corpus[i - 1]
            l_dict = SK_trigram.get(key, {})
            if corpus[i] in l_dict:
                SK_trigram[key][corpus[i]] += 1

    # print('prevadim CZ model na pst')
    CZ_trigram = trigram_to_pst(CZ_trigram)
    # print('prevadim SK model na pst')
    SK_trigram = trigram_to_pst(SK_trigram)
    print_to_file('CZ_trigram.txt', CZ_trigram)
    print_to_file('SK_trigram.txt', SK_trigram)





def print_to_json(filename, trigram):
    # print("zapisuji: " + filename)
    fo = open(filename, "w", encoding='UTF-8')
    json_txt = json.dumps(trigram)
    fo.write(json_txt)
    fo.close()

def print_to_file(filename, trigram):
    # print("zapisuji: " + filename)
    fo = open(filename, "w", encoding='UTF-8')
    for key, value in trigram.items():
        for key2, value2 in value.items():

            fo.write(key + key2 + ' >>> ' + str(value2) + '\n')
    fo.close()


def trigram_to_pst(trigram):
    for first_key, dict in trigram.items():
        total_count = 0
        for key, value in dict.items():
            if trigram[first_key][key] == 0:
                trigram[first_key][key] = 1
            total_count += trigram[first_key][key]
        for key, value in dict.items():
            trigram[first_key][key] /= total_count
    return trigram


def lang(filename):
    global CZ_trigram
    global SK_trigram
    with open(filename, 'r', encoding='windows-1250', errors='ignore') as f:
        txt = f.read()
        lang_txt(txt)


def escape_read(txt):
    global alphabet
    new_c = []
    alphabet_changed = False
    txt = txt.replace('#', '').lower()
    for c in txt:
        if not (c in alphabet):
            alphabet.append(c)
            new_c.append(c)
            alphabet_changed = True
    if alphabet_changed:
        global CZ_trigram
        global SK_trigram
        print('nalezena neznama pismena')
        print(new_c)
        print('nutno vytvorit novy model a pretrenovat ho')
        learn_trigram(CZ_corpus, SK_corpus)
    return txt


def pst(y, trigram):
    p_result = 1
    for triple in y:
        # print(triple[0:2]+triple[2])
        try:
            value = trigram[triple[0:2]][triple[2]]
            logvalue = math.log(value)
            p_result += logvalue
        except:
            p_result
            #print('chyba pst - chybi index')
    # print(p_result)
    return p_result


def lang_txt(txt):
    global CZ_trigram
    global SK_trigram
    txt = txt.lower()
    txt = escape_read(txt)
    txt = '##'+txt
    y = []
    for x in range(len(txt) - 3):
        n = txt[x:x + 3]
        y.append(n)
    cz_pst = pst(y, CZ_trigram)
    sk_pst = pst(y, SK_trigram)

    print('CZ: '+str(cz_pst))
    print('SK: '+str(sk_pst))

    if cz_pst > sk_pst :
        print('----> cesky text')
    else:
        print('----> slovensky text')


print('vytvarim model')
make_alphabet(CZ_corpus, SK_corpus)
print('trenuji a vyhlazuji model')


learn_trigram(CZ_corpus, SK_corpus)


print('zkouseni na vstupnich datech')
print('------------------------------------------------')
print('idnes1.txt')
lang('./data/idnes1.txt')
print('idnes2.txt')
lang('./data/idnes2.txt')
print('idnes3.txt')
lang('./data/idnes3.txt')
print('idnes4.txt')
lang('./data/idnes4.txt')
print('idnes5.txt')
lang('./data/idnes5.txt')
print('zozname1.txt')
lang('./data/zoznam1.txt')
print('zozname2.txt')
lang('./data/zoznam2.txt')
print('zozname3.txt')
lang('./data/zoznam3.txt')
print('zozname4.txt')
lang('./data/zoznam4.txt')
print('zozname5.txt')
lang('./data/zoznam5.txt')


# print('ctk1.txt')
# lang('./data/ctk1.txt')

# print('-----------------------')
# lang_txt('Bydlet půl roku v ústeckém ghettu Předlice se rozhodla od června Eva Fialová, ústecká radní a poslankyně za hnutí ANO. Rozhodla se před měsícem po výjezdním zasedání výboru pro veřejnou správu ')
#
# lang_txt('Celkovo 15 ľudí vrátane šiestich detí prišlo o život po tom, ako počas razie srílanských bezpečnostných zložiek došlo k prestrelke s militantmi prepojenými na nedeľné útoky, pričom traja samovražední atentátnici sa vyhodili do vzduchu. Informovala o tom agentúra AFP s odvolaním sa na miestnu políciu.')
print('-----------------------')
txt = input("Zadejte text(pro ukonceni nechte prazdne): ")

while txt != '':
    lang_txt(txt)
    itxt = input("Zadejte text(pro ukonceni nechte prazdne): ")


# CZ_corpus = 'bd_CZ.txt'
# SK_corpus = 'bd_SK.txt'
#
# print('BD vytvarim model')
# make_alphabet(CZ_corpus, SK_corpus)
# print('BD trenuji a vyhlazuji model')
#
#
# learn_trigram(CZ_corpus, SK_corpus)
#
#
# print('zkouseni na vstupnich datech')
# print('------------------------------------------------')
# print('bd_idnes1.txt')
# lang('./data/bd_idnes1.txt')
# print('bd_idnes2.txt')
# lang('./data/bd_idnes2.txt')
# print('bd_idnes3.txt')
# lang('./data/bd_idnes3.txt')
# print('bd_idnes4.txt')
# lang('./data/bd_idnes4.txt')
# print('bd_idnes5.txt')
# lang('./data/bd_idnes5.txt')
# print('bd_zozname1.txt')
# lang('./data/bd_zoznam1.txt')
# print('bd_zozname2.txt')
# lang('./data/bd_zoznam2.txt')
# print('bd_zozname3.txt')
# lang('./data/bd_zoznam3.txt')
# print('bd_zozname4.txt')
# lang('./data/bd_zoznam4.txt')
# print('bd_zozname5.txt')
# lang('./data/bd_zoznam5.txt')
