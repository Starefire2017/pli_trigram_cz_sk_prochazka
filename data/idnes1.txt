"Neexistuje element�rn� vyjasn�n�, kdo m� jmenovat odborn�ky na jak� resort. To mus� b�t zcela z�ejm�. Po��dal jsem premi�ra Fischera, aby svolal p�edsedy koali�n�ch stran, abychom o tomto rozd�len� vedli jedn�n�," prohl�sil dopoledne Martin Burs�k.

Zelen� navrhli na ministerstvo �ivotn�ho prost�ed� Ladislava Mika, na �kolstv� Jakuba D�rra a na kulturu Martu Smol�kovou. - profily kandid�t� do vl�dy �t�te zde

SMS zdarma

Pokud si objedn�te SMS zpravodajstv� port�lu iDNES.cz, budete m�t slu�bu prvn� m�s�c zdarma a potom za 29 K�/m�s�c. Plat� pro z�kazn�ky O2. V�ce zde

Jen�e na �kolstv� a kulturu neform�ln� navrhla sv� lidi i ODS. Ladislava Mika zase kv�li �dajn� nekompetentnosti kritizoval ��f socialist� Ji�� Paroubek.

A podle �TK si p�edseda �SSD n�rokuje i obsazen� funkce ministra kultury. Post by mohl zast�vat �editel �esk� filharmonie V�clav Riedlbauch.

Zelen� by tak ve vl�d� nemuseli m�t v�bec ��dn�ho z trojice nestran�k�, kterou navrhli. I bez jejich hlas� by ov�em kabinet - z�sk�-li podporu ODS a �SSD - m�l ve Sn�movn� pohodlnou v�t�inu. Naplnily by se tak p�edpov�di, �e vznikne v podstat� velk� koalice dvou nejsiln�j��ch stran.

Burs�k p�ipustil, �e by jeho strana mohla couvnout ze t�� na dva ministersk� posty. Pokud se politici nedohodnou, zelen� se podle n�j z vl�dn�ho projektu zcela st�hnou.

OBT͎E P�IPUSTIL BURS�K U� V�ERA

Martin Burs�k v�era po sch�zce s Janem Fischerem, p�i kter� designovan� premi�r zavr�il sv� konzulta�n� kole�ko, p�ipustil, �e jedn�n� byla obt�n�. "N�kter� v�ci bude t�eba doladit," �ekl na odchodu z Hrz�nsk�ho pal�ce. - v�ce o jedn�n� �t�te zde

Podle Paroubka nen� nezbytn�, aby se Fischer rozhodl dnes. "D�le�it� je, aby vl�da byla jmenov�na 8. kv�tna," �ekl �TK. �e dnes vl�da nebude, p�ipustil i Fischer�v mluv�� Roman Prorok. "Jako prvn� by pan premi�r informoval vybran� kandid�ty, pak pana prezidenta a teprve pak by se to dozv�d�la m�dia. Mysl�m, �e dnes v�razn� posun nenastane," �ekl.
Fischer: Domluvte se mezi politiky

Designovan� premi�r Jan Fischer sch�zku ale nesvol�. "Premi�r zast�v� n�zor, �e jsou to politici, kte�� by m�li dosp�t ke vz�jemn� dohod�," odk�zal Burs�ka na p�edsedy ODS a �SSD Fischer�v mluv�� Roman Prorok.

Z�rove� dodal, �e pokud by se zelen� st�hli, "pan premi�r si um� p�edstavit, �e by ministerstvo �ivotn�ho prost�ed� obsadil kandid�t navr�en� ODS nebo �SSD".
�SSD: Nic nebude, nenech�me se vyd�rat

Na briefing �ty��lenn�ho klubu zelen�ch reagoval s�lov�m v�stupem v tiskov�m centru Sn�movny p�edseda �SSD Ji�� Paroubek. "Jsem znepokojen zm�nou postoj� Burs�kov�ch zelen�ch. Pokud se pan Burs�k vydal cestou pana �unka, kter� nen� schopen dodr�ovat dohody, tak je to jen jeden ze smutn�ch miln�k� zelen�ch v politice," �ekl.

Po�adavek Burs�ka odm�tl s t�m, �e je p�emr�t�n�. "Nikdo nem��e po��tat s t�m, �e bych revidoval n�jak� dohody, kter� byly u�in�ny p�edt�m. Po�adavek na t�i ministry je nep�im��en� jejich politick�mu vlivu, pan Burs�k by m�l p�estat vyd�rat. Chce-li ministry ve vl�d�, m�l by vyhr�t volby," doporu�il zelen�m.

Na ot�zku, zda by �SSD �la i do projektu s ministry navr�en�mi jen soci�ln�mi a ob�ansk�mi demokraty, odm�tl Paroubek zat�m odpov�d�t.
ODS lobbovala za Koc�ba

Cel� ��ast zelen�ch na projektu p�eklenovac� vl�dy je v�bec nejasn�. ODS navrhla - neform�ln�, jak zd�raznil premi�r Topol�nek - dva kandid�ty na ministerstva, kam cht�j� sv� lidi dostat i zelen�.

A i p�es volbu, kterou t�m p�ipravila Janu Fischerovi, v�era Topol�nek u Fischera orodoval za t���lenn� zastoupen� zelen�ch ve vl�d� a za to, aby v kabinetu z�stal i Michael Koc�b. iDNES.cz to �ekl d�v�ryhodn� zdroj, kter� si nep��l b�t jmenov�n.